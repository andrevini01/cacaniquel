package com.company;

import jdk.nashorn.internal.runtime.OptimisticReturnFilters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.SplittableRandom;

public class CacaNiquel {
    private int quantidadeSlots = 3;

    public void executarCacaNiquel() {
        InputOutput inputOutput = new InputOutput();

        ArrayList<Slots> valoresSorteados = sortearSlots();
        int pontuacao = gerarPontuacao(valoresSorteados);

        inputOutput.imprimirMensagem(valoresSorteados.toString());
        inputOutput.imprimirMensagem(Integer.toString(pontuacao));
    }

    public ArrayList<Slots> sortearSlots() {
        ArrayList<Slots> valoresSorteados = new ArrayList<>();
        Random random = new Random();

        for (int i = 0; i < quantidadeSlots; i++) {
            valoresSorteados.add(
                    Slots.values().clone()[random.nextInt(Slots.values().length)]);
        }

        return valoresSorteados;
    }

    public int gerarPontuacao(ArrayList<Slots> valoresSorteados) {
        int pontuacao = 0;

        for (Slots valorSorteado : valoresSorteados) {
            pontuacao += valorSorteado.getValor();
        }

        if (validarTresSlotsIguais(valoresSorteados)) {
            pontuacao *= 100;
        }

        return pontuacao;
    }

    private boolean validarTresSlotsIguais(ArrayList<Slots> valoresSorteados) {
        int contadorValoresIguais = 0;

        for (Slots slot : valoresSorteados){
            if (slot.equals(valoresSorteados.get(0))){
                contadorValoresIguais++;
            }
        }

        if (contadorValoresIguais == valoresSorteados.size()) {
            return true;
        } else {
            return false;
        }
    }
}
