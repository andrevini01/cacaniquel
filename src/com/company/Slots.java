package com.company;

public enum Slots {
    Banana(10),
    Framboesa(50),
    Moeda(100),
    Sete(300);

    Slots(int valor) {
        this.valor = valor;
    }

    int valor;

    public int getValor() {
        return valor;
    }
}
